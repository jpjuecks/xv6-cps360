#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

void

quit_with_error(char *errmsg){
  printf(1, "%s", errmsg);
  exit(0);

}


int
main(int argc, char *argv[])
{
  int test;
  char *file_text = "The quick brown fox jumps over the lazy dog.\n";

  if((test = open("seektest.txt", O_CREATE | O_RDWR)) < 0){
    quit_with_error("seektest: cannot create seektest.txt\n");
  }
  write(test, file_text, strlen(file_text));


  // test seek with each flag
  char c;
  seek(test, 0, SEEK_SET);
  if(read(test, &c, 1) && c != 'T'){
	close(test);
    quit_with_error("seektest: unexpected character in test\n");
  }

  seek(test, 13, SEEK_END);
  if(read(test, &c, 1) && c != 't'){
	close(test);
    quit_with_error("seektest: unexpected character in test\n");
  }

  seek(test, 5, SEEK_CUR);
  if(read(test, &c, 1) && c != 'z'){
	close(test);
    quit_with_error("seektest: unexpected character in test\n");
  }

  close(test);


  // test truncation
  if((test = open("seektest.txt", O_TRUNC)) < 0){
    quit_with_error("seektest: cannot create seektest.txt\n");
  }


  struct stat st;
  fstat(test, &st);
  if (st.size != 0){
	close(test);
    quit_with_error("seektest: size is not 0 after truncating.\n");

  }
  
  close(test);

  if(unlink("seektest.txt") < 0){
    quit_with_error("seektest: error deleting test file");
  }
  printf(1, "All seek tests passed!\n");
  exit(0);

}
