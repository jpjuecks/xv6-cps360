#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[]) {
  if(argc < 2){
    printf(2, "Usage: unmount <mnt_point>\n");
    return 0;
  }
  int i = unmount(argv[1]);
  if(i < 0){
  	printf(2, "unmount failed.\n");
  }
  return 0;
}
